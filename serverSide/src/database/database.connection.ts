import 'reflect-metadata';
import { createConnection, Connection, ConnectionOptions } from 'typeorm';
import { join } from 'path';
const parentDir = join(__dirname, '..');

const connectionOpts: ConnectionOptions = {
   //Coloque sus credenciales aqu�
    entities: [
        `${parentDir}/entities/**/*.ts`,
        `${parentDir}/entities/**/*.js`,
    ],
    synchronize: true,
    type: "mysql",
    host: "localhost",
    port: 3306,
    username: "usuario",
    password: "admin123",
    database: "registrovacunacion",
    logging: false
};

const connection: Promise<Connection> = createConnection(connectionOpts);

export default connection;