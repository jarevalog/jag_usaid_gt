import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { JaEfectos } from "./JaEfectos";
import { JaVacunacion } from "./JaVacunacion";

@Index("fk_efectos_vacunacion_efectos1_idx", ["efectosId"], {})
@Index("fk_efectos_vacunacion_vacunacion1_idx", ["vacunacionId"], {})
@Entity("ja_efectos_vacunacion", { schema: "registrovacunacion" })
export class JaEfectosVacunacion {
  @PrimaryGeneratedColumn({ type: "int", name: "id" })
  id: number;

  @Column("int", { name: "efectos_id" })
  efectosId: number;

  @Column("int", { name: "vacunacion_id", nullable: true })
  vacunacionId: number | null;

  @Column("varchar", { name: "otro", nullable: true, length: 500 })
  otro: string | null;

  @ManyToOne(() => JaEfectos, (jaEfectos) => jaEfectos.jaEfectosVacunacions, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "efectos_id", referencedColumnName: "id" }])
  efectos: JaEfectos;

  @ManyToOne(
    () => JaVacunacion,
    (jaVacunacion) => jaVacunacion.jaEfectosVacunacions,
    { onDelete: "NO ACTION", onUpdate: "NO ACTION" }
  )
  @JoinColumn([{ name: "vacunacion_id", referencedColumnName: "id" }])
  vacunacion: JaVacunacion;
}
