import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class tc_color {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @Column()
    fecha_creacion: Date;

    @Column()
    vigente: string;

    @Column()
    fecha_inhabilitacion: Date;

}
