import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { JaVacunacion } from "./JaVacunacion";

@Entity("ja_vacuna", { schema: "registrovacunacion" })
export class JaVacuna {
  @PrimaryGeneratedColumn({ type: "int", name: "id" })
  id: number;

  @Column("varchar", { name: "nombre_vacuna", length: 255 })
  nombreVacuna: string;

  @Column("varchar", { name: "procedencia", nullable: true, length: 45 })
  procedencia: string | null;

  @OneToMany(() => JaVacunacion, (jaVacunacion) => jaVacunacion.vacuna)
  jaVacunacions: JaVacunacion[];
}
