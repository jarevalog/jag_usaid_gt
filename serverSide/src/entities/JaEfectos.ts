import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { JaEfectosVacunacion } from "./JaEfectosVacunacion";

@Entity("ja_efectos", { schema: "registrovacunacion" })
export class JaEfectos {
  @PrimaryGeneratedColumn({ type: "int", name: "id" })
  id: number;

  @Column("varchar", {
    name: "efecto",
    length: 255,
    default: () => "'posible efecto'",
  })
  efecto: string;

  @OneToMany(
    () => JaEfectosVacunacion,
    (jaEfectosVacunacion) => jaEfectosVacunacion.efectos
  )
  jaEfectosVacunacions: JaEfectosVacunacion[];
}
