import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { JaEfectosVacunacion } from "./JaEfectosVacunacion";
import { JaVacuna } from "./JaVacuna";
import { JaPersona } from "./JaPersona";

@Index("fk_vacunacion_ja_persona1_idx", ["personaCui"], {})
@Index("fk_vacunacion_ja_vacuna1_idx", ["vacunaId"], {})
@Entity("ja_vacunacion", { schema: "registrovacunacion" })
export class JaVacunacion {
  @PrimaryGeneratedColumn({ type: "int", name: "id" })
  id: number;

  @Column("int", { name: "embarazo" })
  embarazo: number;

  @Column("datetime", { name: "fechaParto", nullable: true })
  fechaParto: Date | null;

  @Column("int", { name: "dosis", nullable: true })
  dosis: number | null;

  @Column("int", { name: "persona_cui" })
  personaCui: number;

  @Column("int", { name: "vacuna_id" })
  vacunaId: number;

  @Column("timestamp", {
    name: "fecha_vacunacion",
    nullable: true,
    default: () => "CURRENT_TIMESTAMP",
  })
  fechaVacunacion: Date | null;

  @OneToMany(
    () => JaEfectosVacunacion,
    (jaEfectosVacunacion) => jaEfectosVacunacion.vacunacion
  )
  jaEfectosVacunacions: JaEfectosVacunacion[];

  @ManyToOne(() => JaVacuna, (jaVacuna) => jaVacuna.jaVacunacions, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "vacuna_id", referencedColumnName: "id" }])
  vacuna: JaVacuna;

  @ManyToOne(() => JaPersona, (jaPersona) => jaPersona.jaVacunacions, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "persona_cui", referencedColumnName: "cui" }])
  personaCui2: JaPersona;
}
