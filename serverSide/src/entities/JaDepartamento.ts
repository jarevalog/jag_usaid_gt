import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { JaMunicipio } from "./JaMunicipio";

@Entity("ja_departamento", { schema: "registrovacunacion" })
export class JaDepartamento {
  @PrimaryGeneratedColumn({ type: "int", name: "id" })
  id: number;

  @Column("varchar", { name: "departamento", length: 255 })
  departamento: string;

  @OneToMany(() => JaMunicipio, (jaMunicipio) => jaMunicipio.departamento)
  jaMunicipios: JaMunicipio[];
}
