import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from "typeorm";
import { JaMunicipio } from "./JaMunicipio";
import { JaVacunacion } from "./JaVacunacion";

@Index("fk_persona_municipio1_idx", ["municipioId"], {})
@Entity("ja_persona", { schema: "registrovacunacion" })
export class JaPersona {
  @Column("int", { primary: true, name: "cui" })
  cui: number;

  @Column("varchar", { name: "nombres", length: 255 })
  nombres: string;

  @Column("varchar", { name: "apellidos", nullable: true, length: 255 })
  apellidos: string | null;

  @Column("datetime", { name: "fecha_nacimiento", nullable: true })
  fechaNacimiento: Date | null;

  @Column("varchar", { name: "telefono", nullable: true, length: 45 })
  telefono: string | null;

  @Column("int", { name: "municipio_id" })
  municipioId: number;
  
  @Column("varchar", { name: "sexo", length: 1 })
  sexo: string;
  
  @ManyToOne(() => JaMunicipio, (jaMunicipio) => jaMunicipio.jaPersonas, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "municipio_id", referencedColumnName: "id" }])
  municipio: JaMunicipio;

  @OneToMany(() => JaVacunacion, (jaVacunacion) => jaVacunacion.personaCui2)
  jaVacunacions: JaVacunacion[];
}
