import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { JaDepartamento } from "./JaDepartamento";
import { JaPersona } from "./JaPersona";

@Index("fk_municipio_departamento_idx", ["departamentoId"], {})
@Entity("ja_municipio", { schema: "registrovacunacion" })
export class JaMunicipio {
  @PrimaryGeneratedColumn({ type: "int", name: "id" })
  id: number;

  @Column("varchar", { name: "municipio", length: 255 })
  municipio: string;

  @Column("int", { primary: true, name: "departamento_id" })
  departamentoId: number;

  @ManyToOne(
    () => JaDepartamento,
    (jaDepartamento) => jaDepartamento.jaMunicipios,
    { onDelete: "NO ACTION", onUpdate: "NO ACTION" }
  )
  @JoinColumn([{ name: "departamento_id", referencedColumnName: "id" }])
  departamento: JaDepartamento;

  @OneToMany(() => JaPersona, (jaPersona) => jaPersona.municipio)
  jaPersonas: JaPersona[];
}
