import * as Koa from 'koa';
import * as Router from 'koa-router';
import { getRepository, Repository } from 'typeorm';
import { tc_color } from '../entities/catalogos/tc_color';
import * as HttpStatus from 'http-status-codes';

const routerOpts: Router.IRouterOptions = {
    prefix: '/catalogos',
};

const router: Router = new Router(routerOpts);

router.get('/', async (ctx: Koa.Context) => {
    const colorRepo: Repository<tc_color> = getRepository(tc_color);
    const colores = await colorRepo.find();

    ctx.body = {
         colores ,
    };
});

router.get('/:id_color', async (ctx: Koa.Context) => {
    const colorRepo: Repository<tc_color> = getRepository(tc_color);
    const color = await colorRepo.findOne(ctx.params.id_color);

    if (!color) {
        ctx.throw(HttpStatus.NOT_FOUND);
    }
    ctx.body = {
         color ,
    };
});

router.post('/', async (ctx: Koa.Context) => {
    const colorRepo: Repository<tc_color> = getRepository(tc_color);
    const color = await colorRepo.create(ctx.request.body);
    await colorRepo.save(color);

    ctx.body = {
         color ,
    };
});

router.delete('/:id_color', async (ctx: Koa.Context) => {
    const colorRepo: Repository<tc_color> = getRepository(tc_color);
    const color = await colorRepo.findOne(ctx.params.id_color);

    if (!color) {
        ctx.throw(HttpStatus.NOT_FOUND);
    }
    await colorRepo.delete(color);
});

router.put('/:id_color', async (ctx: Koa.Context) => {
    const colorRepo: Repository<tc_color> = getRepository(tc_color);
    const color = await colorRepo.findOne(ctx.params.id_color);

    if (!color) {
        ctx.throw(HttpStatus.NOT_FOUND);
    }

    const updatedColor = await colorRepo.merge(color, ctx.request.body);

    // Save the new data.
    colorRepo.save(updatedColor);
    ctx.body = {
         movie: updatedColor ,
    };
});

export default router;