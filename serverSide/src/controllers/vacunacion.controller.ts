import * as Koa from 'koa';
import * as Router from 'koa-router';
import { getRepository, Repository } from 'typeorm';
import * as HttpStatus from 'http-status-codes';
import { JaVacunacion } from './../entities/JaVacunacion';

const routerOpts: Router.IRouterOptions = {
    prefix: '/vacunaciones',
};

const router: Router = new Router(routerOpts);

router.get('/', async (ctx: Koa.Context) => {
    const repo: Repository<JaVacunacion> = getRepository(JaVacunacion);
    const registros = await repo.find({
        relations: ['personaCui2','vacuna','jaEfectosVacunacions']
        });
    
    ctx.body = {
         registros ,
    };
});

router.get('/:persona_cui', async (ctx: Koa.Context) => {
    const repo: Repository<JaVacunacion> = getRepository(JaVacunacion);
    const registros = await repo.find({"personaCui":ctx.params.persona_cui});
    ctx.body = {
         registros ,
    };
});

router.get('/byId/:id', async (ctx: Koa.Context) => {
    const repo: Repository<JaVacunacion> = getRepository(JaVacunacion);
    const registros = await repo.findOne(ctx.params.id);
    ctx.body = {
         registros ,
    };
});

router.post('/', async (ctx: Koa.Context) => {
    
    const repo: Repository<JaVacunacion> = getRepository(JaVacunacion);
    const registro = await repo.create(ctx.request.body);
    console.log('JaVacunacion post ')
    await repo.save(registro);
    ctx.body = {
         registro ,
    };
});

router.delete('/:id', async (ctx: Koa.Context) => {
    const repo: Repository<JaVacunacion> = getRepository(JaVacunacion);
    const registro = await repo.findOne(ctx.params.id);

    /*if (!color) {
        ctx.throw(HttpStatus.NOT_FOUND);
    }*/
    await repo.delete(registro);
    ctx.body = {
    registro,
   };
});

export default router;