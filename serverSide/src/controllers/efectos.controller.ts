import * as Koa from 'koa';
import * as Router from 'koa-router';
import { getRepository, Repository } from 'typeorm';
import * as HttpStatus from 'http-status-codes';
import { JaEfectos } from './../entities/JaEfectos';

const routerOpts: Router.IRouterOptions = {
    prefix: '/efectos',
};

const router: Router = new Router(routerOpts);

router.get('/', async (ctx: Koa.Context) => {
    const repo: Repository<JaEfectos> = getRepository(JaEfectos);
    const registros = await repo.find();

    ctx.body = {
        registros,
    };
});

export default router;