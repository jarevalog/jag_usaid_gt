import * as Koa from 'koa';
import * as Router from 'koa-router';
import { getRepository, Repository } from 'typeorm';
import { JaDepartamento } from '../entities/JaDepartamento';
import * as HttpStatus from 'http-status-codes';

const routerOpts: Router.IRouterOptions = {
    prefix: '/departamentos',
};

const router: Router = new Router(routerOpts);

router.get('/', async (ctx: Koa.Context) => {
    const repo: Repository<JaDepartamento> = getRepository(JaDepartamento);
    const registros = await repo.find();

    ctx.body = {
        registros,
    };
});

export default router;