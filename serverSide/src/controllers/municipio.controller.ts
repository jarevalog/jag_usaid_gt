import { JaMunicipio } from "../entities/JaMunicipio";
import * as Koa from "koa";
import * as Router from "koa-router";
import { getRepository, Repository } from "typeorm";
import * as HttpStatus from "http-status-codes";

const routerOpts: Router.IRouterOptions = {
  prefix: "/municipios",
};

const router: Router = new Router(routerOpts);

router.get("/", async (ctx: Koa.Context) => {
  const repo: Repository<JaMunicipio> = getRepository(JaMunicipio);
  const registros = await repo.find({
    relations: ["departamento"],
  });

  ctx.body = {
    registros,
  };
});

router.get("/:departamentoId", async (ctx: Koa.Context) => {
  const repo: Repository<JaMunicipio> =
    getRepository(JaMunicipio);
  const registros = await repo.find({ departamentoId: ctx.params.departamentoId });
  ctx.body = {
    registros,
  };
});

export default router;
