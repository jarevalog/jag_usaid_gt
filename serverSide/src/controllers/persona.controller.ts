import * as Koa from 'koa';
import * as Router from 'koa-router';
import { getRepository, Repository } from 'typeorm';
import * as HttpStatus from 'http-status-codes';
import { JaPersona } from './../entities/JaPersona';

const routerOpts: Router.IRouterOptions = {
    prefix: '/personas',
};

const router: Router = new Router(routerOpts);

router.get('/', async (ctx: Koa.Context) => {
    const repo: Repository<JaPersona> = getRepository(JaPersona);
    const registros = await repo.find({
        relations: ['municipio','municipio.departamento','jaVacunacions']
        });
    
    ctx.body = {
         registros ,
    };
});

router.get('/:cui', async (ctx: Koa.Context) => {
    const repo: Repository<JaPersona> = getRepository(JaPersona);
    const registro = await repo.findOne(ctx.params.cui,{
        relations: ['municipio','municipio.departamento','jaVacunacions']
        });

   /*  if (!registro) {
        ctx.body;
    } */
    ctx.body = {
         registro ,
    };
});

router.post('/', async (ctx: Koa.Context) => {
    const repo: Repository<JaPersona> = getRepository(JaPersona);
    const registro = await repo.create(ctx.request.body);
    await repo.save(registro);

    ctx.body = {
         registro ,
    };
});

router.delete('/:cui', async (ctx: Koa.Context) => {
    const repo: Repository<JaPersona> = getRepository(JaPersona);
    const registro = await repo.findOne(ctx.params.cui);

    if (!registro) {
        ctx.throw(HttpStatus.NOT_FOUND);
    }
    await repo.delete(registro);
});

router.put('/:cui', async (ctx: Koa.Context) => {
    const repo: Repository<JaPersona> = getRepository(JaPersona);
    const registro = await repo.findOne(ctx.params.cui);

    if (!registro) {
        ctx.throw(HttpStatus.NOT_FOUND);
    }

    const updatedRegistro = await repo.merge(registro, ctx.request.body);

    // Save the new data.
    repo.save(updatedRegistro);
    ctx.body = {
         updatedRegistro ,
    };
});

export default router;