import * as Koa from 'koa';
import * as Router from 'koa-router';
import { getRepository, Repository } from 'typeorm';
import { user } from '../entities/catalogos/user';
import * as HttpStatus from 'http-status-codes';

const routerOpts: Router.IRouterOptions = {
    prefix: '/users',
};

const router: Router = new Router(routerOpts);

router.get('/', async (ctx: Koa.Context) => {
    const userRepo: Repository<user> = getRepository(user);
    const users = await userRepo.find();

    ctx.body = {
        data: { users },
    };
});

export default router;