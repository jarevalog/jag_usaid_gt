import * as Koa from 'koa';
import * as Router from 'koa-router';
import { getRepository, Repository } from 'typeorm';
import * as HttpStatus from 'http-status-codes';
import { JaVacuna } from './../entities/JaVacuna';

const routerOpts: Router.IRouterOptions = {
    prefix: '/vacunas',
};

const router: Router = new Router(routerOpts);

router.get('/', async (ctx: Koa.Context) => {
    const repo: Repository<JaVacuna> = getRepository(JaVacuna);
    const registros = await repo.find();

    ctx.body = {
        registros,
    };
});

router.get('/:id', async (ctx: Koa.Context) => {
    const repo: Repository<JaVacuna> = getRepository(JaVacuna);
    const registro = await repo.findOne(ctx.params.id);
   /*  if (!registro) {
        ctx.body;
    } */
    ctx.body = {
         registro ,
    };
});
export default router;