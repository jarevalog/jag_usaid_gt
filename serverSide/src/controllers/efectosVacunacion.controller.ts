import * as Koa from "koa";
import * as Router from "koa-router";
import { getRepository, Repository } from "typeorm";
import * as HttpStatus from "http-status-codes";
import { JaEfectosVacunacion } from "../entities/JaEfectosVacunacion";

const routerOpts: Router.IRouterOptions = {
  prefix: "/efectosVacuna",
};

const router: Router = new Router(routerOpts);

router.get("/", async (ctx: Koa.Context) => {
  const repo: Repository<JaEfectosVacunacion> =
    getRepository(JaEfectosVacunacion);
  const registros = await repo.find();

  ctx.body = {
    registros,
  };
});

router.get("/:vacunacionId", async (ctx: Koa.Context) => {
  const repo: Repository<JaEfectosVacunacion> =
    getRepository(JaEfectosVacunacion);
  const registros = await repo.find({ vacunacionId: ctx.params.vacunacionId });
  ctx.body = {
    registros,
  };
});

router.post("/", async (ctx: Koa.Context) => {
  const repo: Repository<JaEfectosVacunacion> =
    getRepository(JaEfectosVacunacion);
  const registro = await repo.create(ctx.request.body);
  console.log("JaEfectosVacunacion post ");
  await repo.save(registro);
  ctx.body = {
    registro,
  };
});

router.delete("/:vacunacionId", async (ctx: Koa.Context) => {
  const repo: Repository<JaEfectosVacunacion> =
    getRepository(JaEfectosVacunacion);
  const registros = await repo.find({ vacunacionId: ctx.params.vacunacionId });
  for (let i of registros) {
    console.log('---------> '+i)
    await repo.delete(i.id);
  }
  let rmap =new Map([
    ['registro', '{}']
  ]);;
  ctx.body = {
    rmap,
  };
});
export default router;
