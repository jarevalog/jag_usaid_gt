import * as Koa from 'koa';
import * as bodyParser from 'koa-bodyparser';
import * as HttpStatus from 'http-status-codes';
import colorController from '../controllers/color.controller';
import userController from '../controllers/user.controller';
import departamentoController from "../controllers/departamento.controller"
import municipioController from "../controllers/municipio.controller"
import personaController from "../controllers/persona.controller"
import vacunaController from "../controllers/vacuna.controller"
import efectosController from "../controllers/efectos.controller"
import vacunacionController from "../controllers/vacunacion.controller"
import efectosVacunacion from '../controllers/efectosVacunacion.controller'

const app: Koa = new Koa();
const cors = require('@koa/cors');

// Generic error handling middleware.
app.use(async (ctx: Koa.Context, next: () => Promise<any>) => {
    try {
        await next();
    } catch (error) {
        ctx.status = error.statusCode || error.status || HttpStatus.INTERNAL_SERVER_ERROR;
        error.status = ctx.status;
        ctx.body = { error };
        ctx.app.emit('error', error, ctx);
    }
});

app.use(bodyParser());
app.use(colorController.routes());
app.use(colorController.allowedMethods());
app.use(userController.routes());
app.use(userController.allowedMethods());
app.use(departamentoController.routes());
app.use(departamentoController.allowedMethods());
app.use(municipioController.routes());
app.use(municipioController.allowedMethods());
app.use(personaController.routes());
app.use(personaController.allowedMethods());
app.use(vacunaController.routes());
app.use(vacunaController.allowedMethods());
app.use(efectosController.routes());
app.use(efectosController.allowedMethods());
app.use(vacunacionController.routes());
app.use(vacunacionController.allowedMethods());
app.use(efectosVacunacion.routes());
app.use(efectosVacunacion.allowedMethods());
app.use(cors());

// Application error logging.
app.on('error', console.error);

export default app;