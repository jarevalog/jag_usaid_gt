function calculate_age(birth_month, birth_day, birth_year) {
  today_date = new Date();
  today_year = today_date.getFullYear();
  today_month = today_date.getMonth();
  today_day = today_date.getDate();
  age = today_year - birth_year;

  if (today_month < birth_month - 1) {
    age--;
  }
  if (birth_month - 1 == today_month && today_day < birth_day) {
    age--;
  }
  return age;
}
function monthDiff(d1, d2) {
  var months;
  months = (d2.getFullYear() - d1.getFullYear()) * 12;
  months -= d1.getMonth();
  months += d2.getMonth();

  return months <= 0 ? 0 : months;
}
function testDates(f1, f2) {
  var dia = f1.split("/")[2];
  var mes = f1.split("/")[1];
  var anio = f1.split("/")[0];
  d1 = new Date(anio, mes, dia);
  var dia = f2.split("/")[2];
  var mes = f2.split("/")[1];
  var anio = f2.split("/")[0];
  d2 = new Date(anio, mes, dia);
  var diff = monthDiff(d1, d2);
  return diff;
}
