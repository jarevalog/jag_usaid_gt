import { NoEntrontradoComponent } from './componentes/no-entrontrado/no-entrontrado.component';
import { RegistraEfectoComponent } from './componentes/registra-efecto/registra-efecto.component';
import { RegistraPersonaComponent } from './componentes/registra-persona/registra-persona.component';
import { ResultadoComponent } from './componentes/consulta/resultado/resultado.component';
import { ConsultaComponent } from './componentes/consulta/consulta.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistrarComponent } from './componentes/registrar/registrar.component';
import { InicioComponent } from './componentes/inicio/inicio.component';
const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'inicio', component: InicioComponent},
  {path: 'consulta', component: ConsultaComponent},
  {path: 'registro', component: RegistrarComponent},
  {path: 'resultado/:cui', component: ResultadoComponent},
  {path: 'registroPersona', component: RegistraPersonaComponent},
  {path: 'registroEfecto/:cui', component: RegistraEfectoComponent},
  {path: 'noEncontrado', component: NoEntrontradoComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
