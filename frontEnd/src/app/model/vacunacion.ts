export class VacunacionI {
  id: number;
  embarazo: string;
  fechaParto: string;
  dosis: number;
  personaCui: number;
  vacunaId: number;
  fechaVacunacion: string;

  constructor(
    id: number,
    embarazo: number,
    fechaParto: string,
    dosis: number,
    personaCui: number,
    vacunaId: number,
    fechaVacunacion: string
  ) {
    this.id = id;
    this.embarazo = embarazo == 0 ? 'No' : 'Si';
    this.fechaParto = fechaParto!=null?(fechaParto.length>9?fechaParto.substr(0, 10):fechaParto):'';
    this.dosis = dosis;
    this.personaCui = personaCui;
    this.vacunaId = vacunaId;
    this.fechaVacunacion = fechaVacunacion!=null?(fechaVacunacion.length>9?fechaVacunacion.substr(0, 10):fechaVacunacion):'';
  }

  setVacunaId(id: number) {
    this.id = id;
  }
  
}
