export class genericOptsSelect {
  public id!: number;
  public nombre!: string;
  public nombreControl!: string;
  constructor(id: number, nombre: string,control:string) {
    this.id = id;
    this.nombre = nombre;
    this.nombreControl=control
  }
}
