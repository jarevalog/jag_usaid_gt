import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class EfectosService {
  AUTH_SERVER: string = 'http://localhost:3000/efectos';
  constructor(private http: HttpClient) { }

   getEfectos()
   {
     return this.http.get(this.AUTH_SERVER);
   }

 
}
