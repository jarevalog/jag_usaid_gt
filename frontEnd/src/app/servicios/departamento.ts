import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class DepartamentoService {
  AUTH_SERVER: string = 'http://localhost:3000/departamentos';
  constructor(private http: HttpClient) { }

   getDeptos()
   {
     return this.http.get(this.AUTH_SERVER);
   }
}
