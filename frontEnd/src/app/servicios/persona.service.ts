import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class PersonaService {
  AUTH_SERVER: string = 'http://localhost:3000/personas';
  constructor(private http: HttpClient) { }

   getPersonas()
   {
     //return this.http.get(this.url);
   }

   getUnaPersona(cui:number)
   {
     return this.http.get(this.AUTH_SERVER+"/"+cui);
   }

   postPersona(body:any){
    this.http.post<any>(this.AUTH_SERVER, body).subscribe((res) => { 
      console.log('persona registrada')
     });
   }
}
