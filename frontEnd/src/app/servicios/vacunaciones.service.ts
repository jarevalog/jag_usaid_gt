import { EfectosService } from 'src/app/servicios/efectos.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EfectosVacunaService } from 'src/app/servicios/efectosVacuna.service';

@Injectable({
  providedIn: 'root',
})
export class VacunacionesService {
  AUTH_SERVER: string = 'http://localhost:3000/vacunaciones';
  constructor(
    private http: HttpClient,
    private efectosVacunaService: EfectosVacunaService,
    private efectosService: EfectosService
  ) {}

  postVacuna(body: any, arr: any[], otro: boolean, txtOtro: string): number {
    this.http.post<any>(this.AUTH_SERVER, body).subscribe((res) => {
      let newId = res.registro.id;
      let postMap = new Map<string, any>();
      //setear los efectos de las vacunas aca
      console.log('>>> arreglo ' + arr);
      let jsonObject: any = {};
      for (var val of arr) {
        postMap = new Map<string, any>();
        postMap.set('efectosId', parseInt(val));
        postMap.set('vacunacionId', newId);
        postMap.forEach((value, key) => {
          jsonObject[key] = value;
        });
        console.log('antes de enviarlo ' + newId);
        this.efectosVacunaService.postEfectoVacuna(jsonObject);
      }
      if (otro) {
        postMap = new Map<string, any>();
        postMap.set('efectosId', 0);
        postMap.set('vacunacionId', newId);
        postMap.set('otro', txtOtro);
        postMap.forEach((value, key) => {
          jsonObject[key] = value;
        });
        this.efectosVacunaService.postEfectoVacuna(jsonObject);
      }
    });
    return 0;
  }
  async deleteVacunacion(idVacunacion: number) {
    await this.http.delete<any>(this.AUTH_SERVER+"/"+idVacunacion).subscribe(res => { 
      console.log('vacuna eliminada')
    })
  }
}
