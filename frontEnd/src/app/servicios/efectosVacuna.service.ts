import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class EfectosVacunaService {
  AUTH_SERVER: string = 'http://localhost:3000/efectosVacuna';
  constructor(private http: HttpClient) {}

  getUnEfectoVacuna(id: number) {
    return this.http.get(this.AUTH_SERVER + '/' + id);
  }

  postEfectoVacuna(body: any): number {
    this.http.post<any>(this.AUTH_SERVER, body).subscribe((res) => {
      console.log('en servicio efectos vacuna '+JSON.stringify(body))
      return res.registro.id;
    });
    return 0;
  }

  async deleteEfectosVacuna(id:number){
        await this.http.delete<any>(this.AUTH_SERVER+"/"+id).subscribe(res => { 
          console.log('efectos vacuna eliminada')
        })
    /* return new Promise((resolve, reject) => {
      if (isTrue) {
        
      } else {
        reject("Promise rejected");
      }
    });
 */   }
}
