import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class VacunaService {
  AUTH_SERVER: string = 'http://localhost:3000/vacunas';
  constructor(private http: HttpClient) { }

   getVacunas()
   {
     return this.http.get(this.AUTH_SERVER);
   }

   getUnaVacuna(id:number)
   {
     return this.http.get(this.AUTH_SERVER+"/"+id);
   }
   postVacuna(body:any)
   {
     return this.http.post(this.AUTH_SERVER,body);
   }
   
}
