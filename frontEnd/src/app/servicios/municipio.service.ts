import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class MunicipioService {
  AUTH_SERVER: string = 'http://localhost:3000/municipios';
  constructor(private http: HttpClient) { }

   getUnMunicipioByIdDepto(id:number)
   {
     return this.http.get(this.AUTH_SERVER+"/"+id);
   }
}
