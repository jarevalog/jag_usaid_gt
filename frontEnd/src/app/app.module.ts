import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConsultaComponent } from './componentes/consulta/consulta.component';
import { RegistrarComponent } from './componentes/registrar/registrar.component';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ResultadoComponent } from './componentes/consulta/resultado/resultado.component';
import { RegistraEfectoComponent } from './componentes/registra-efecto/registra-efecto.component';
import { RegistraPersonaComponent } from './componentes/registra-persona/registra-persona.component';
import { NoEntrontradoComponent } from './componentes/no-entrontrado/no-entrontrado.component';
import { DatepickerDirective } from './directives/DatepickerDirective';
@NgModule({
  declarations: [
    AppComponent,
    ConsultaComponent,
    RegistrarComponent,
    InicioComponent,
    ResultadoComponent,
    RegistraEfectoComponent,
    RegistraPersonaComponent,
    NoEntrontradoComponent,DatepickerDirective
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
