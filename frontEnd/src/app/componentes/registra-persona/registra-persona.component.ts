import { DepartamentoService } from './../../servicios/departamento';
import { PersonaService } from 'src/app/servicios/persona.service';
import { FormControl, FormGroup, NgForm } from '@angular/forms';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Departamento } from 'src/app/model/departamento';
import { Municipio } from 'src/app/model/municipio';
import { MunicipioService } from 'src/app/servicios/municipio.service';

@Component({
  selector: 'app-registra-persona',
  templateUrl: './registra-persona.component.html',
  styleUrls: ['./registra-persona.component.css'],
})
export class RegistraPersonaComponent implements OnInit {
  map = new Map<string, number>();
  constructor(
    private router: Router,
    private personaService: PersonaService,
    private departamentoService: DepartamentoService,
    private municipioService: MunicipioService
  ) {}

  ngOnInit(): void {
    this.user = new FormGroup({
      fechaNacimiento: new FormControl(''),
      id_departamento: new FormControl(''),
    });
    console.log('init');
    this.departamentoService.getDeptos().subscribe((res) => {
      let arrD: any = res;
      for (let val of arrD.registros) {
        this.departamentos.push(new Departamento(val.id, val.departamento));
        this.map.set(val.departamento, val.id);
      }
    });
    console.log(this.departamentos);
  }
  fechaNacimiento!: string;
  async onRegister(frm: NgForm) {
    let map = new Map<string, string>();
    map.set('cui', frm.value.cui);
    map.set('nombres', frm.value.nombres);
    map.set('apellidos', frm.value.apellidos);
    map.set('fechaNacimiento', frm.value.fechaNacimiento);
    map.set('telefono', frm.value.telefono);
    map.set('municipioId', frm.value.municipioId);
    map.set('sexo', frm.value.optradio);
    let jsonObject: any = {};
    map.forEach((value, key) => {
      jsonObject[key] = value;
    });
    console.log('json ' + JSON.stringify(jsonObject));
    await this.validaciones(map);
    if(this.messages.length<=0){

      this.personaService.postPersona(jsonObject)

      this.router.navigate(['/resultado/' + frm.value.cui]);
    }
  }
  departamentos: Departamento[] = new Array();
  municipios: Municipio[] = new Array();
  departamento!: Departamento;
  //id_departamento!:number
  user!: FormGroup;
  getMunicipios(idDepartamento: any) {
    var idDepto: number = 0;
    idDepto = this.map.get(idDepartamento.value)!;
    console.log('------------------->>> ' + idDepto!);
    this.municipioService.getUnMunicipioByIdDepto(idDepto).subscribe((res) => {
      console.log('municipios');
      let arrD: any = res;
      for (let val of arrD.registros) {
        this.municipios.push(new Municipio(val.id, val.municipio));
      }
    });
  }
  messages: string[] = new Array();
  async validaciones(map: any) {
    this.messages = new Array();
    //validar cui
    let cui = map.get('cui');
    if (cui.length <= 0) {
      this.messages.push('Codigo unico de identificacion es campo requerido');
    } else {
      this.personaService.getUnaPersona(cui).subscribe((res) => {
        var exist = JSON.stringify(res).includes('registro');
        console.log('existe>' + exist);
        if (exist) {
          this.messages.push(
            'Codigo unico de identificacion ya se encuentra registrado'
          );
        }else{
          let isNotNumber:boolean=isNaN(Number(cui))
          console.log("isNotNumber "+isNotNumber)
          if(isNotNumber){
            this.messages.push(
              'Codigo unico de identificacion no es valido'
            );
          }
        }
      });
    }
    let val = map.get('nombres');
    console.log('len val' + val.length);
    if (val.length <= 0) {
      this.messages.push('Nombres es campo requerido');
    }
    val = map.get('apellidos');
    if (val.length <= 0) {
      this.messages.push('Apellidos es campo requerido');
    }
    val = map.get('fechaNacimiento');
    if (val.length <= 0) {
      this.messages.push('Fecha de Nacimiento es campo requerido');
    }
    val = map.get('telefono');
    if (val.length <= 0) {
      this.messages.push('Telefono es campo requerido');
    }else{
      let isNotNumber:boolean=isNaN(Number(val))
          console.log("isNotNumber "+isNotNumber)
          if(isNotNumber){
            this.messages.push(
              'Numero de telefono no es valido'
            );
          }
    }
    val = map.get('sexo');
    if (val.length <= 0) {
      this.messages.push('Sexo es campo requerido');
    }
    val = map.get('municipioId');
    if (val.length <= 0) {
      this.messages.push('Departamento y municipio son campos requeridos');
    }
  }
}
