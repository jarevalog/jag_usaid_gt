import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistraPersonaComponent } from './registra-persona.component';

describe('RegistraPersonaComponent', () => {
  let component: RegistraPersonaComponent;
  let fixture: ComponentFixture<RegistraPersonaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistraPersonaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistraPersonaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
