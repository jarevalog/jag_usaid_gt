import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {NgForm} from '@angular/forms';
import { PersonaService } from 'src/app/servicios/persona.service';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css'],
})
export class ConsultaComponent implements OnInit {
 
  constructor(
    private personaService:PersonaService,
    private router: Router
  ) {}

  ngOnInit(): void {}

   onSearch(form:NgForm): void {
    console.log(form.value.cui)
    this.personaService.getUnaPersona(form.value.cui).subscribe(res => {
     // console.log('LoginComponent '+JSON.stringify(res))
      var exist = JSON.stringify(res).includes("registro")
    if(exist)
    this.router.navigate(['/resultado/'+form.value.cui])
    else
    this.router.navigate(['/noEncontrado'])
  });
  }

}
