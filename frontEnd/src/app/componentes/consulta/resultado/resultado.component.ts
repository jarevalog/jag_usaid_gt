import { VacunacionesService } from 'src/app/servicios/vacunaciones.service';
import { EfectosVacunaService } from 'src/app/servicios/efectosVacuna.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VacunacionI } from 'src/app/model/vacunacion';
import { PersonaService } from 'src/app/servicios/persona.service';
import { VacunaService } from 'src/app/servicios/vacuna.service';
import { EfectosService } from 'src/app/servicios/efectos.service';
import { Router } from '@angular/router';

declare function calculate_age(day:string,month:string,year:string):any

@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.component.html',
  styleUrls: ['./resultado.component.css'],
})
 
 
export class ResultadoComponent implements OnInit {
  constructor(
    private personaService: PersonaService,
    private activeRoute: ActivatedRoute,
    private vacunaService: VacunaService,
    private efectosService: EfectosService,
    private efectosVacunaService: EfectosVacunaService,
    private router: Router,
    private vacunacionesService: VacunacionesService
  ) {}
  gcui!: number;
  edad!:number;
  ngOnInit(): void {
    
    const cui = this.activeRoute.snapshot.params.cui;
    this.gcui = cui;
    //inicializar mapa de vacunas
    this.vacunasMap = new Map([
      [1, 'value1'],
      [2, 'value2'],
    ]);
    this.efectosMap = new Map([
      [1, 'value1'],
      [2, 'value2'],
    ]);
    this.efectosVacunaMap = new Map([[1, new Array()]]);
    //crear objetos tipo mapa de acunas
    this.buildEfectosMap();
    
    //traer personas
    
    // console.log(this.)
  }
  persona: any; //objeto persona
  vacunas: any; //objeto vacunas
  efectos: any; //objeto efectos
  arrVacunaciones: any[] | undefined; //arreglo de vacunaciones

  arr!: VacunacionI[]; //arrego de vacunacion
  getPersona(cui: number): void {
    this.arr = new Array();
    this.personaService.getUnaPersona(cui).subscribe((res) => {
      this.persona = res;
      var dia:string = this.persona.registro.fechaNacimiento.substr(8,2)
      var mes:string = this.persona.registro.fechaNacimiento.substr(5,2)
      var anio:string = this.persona.registro.fechaNacimiento.substr(0,4)
      console.log('nace '+dia+' '+mes+' ' +anio)
      this.edad=calculate_age(dia,mes,anio); 
      console.log(this.edad)
      this.arrVacunaciones = Array.of(this.persona.registro.jaVacunacions);
      for (var val of this.arrVacunaciones[0]) {
        let vi = new VacunacionI(
          val.id,
          val.embarazo,
          val.fechaParto,
          val.dosis,
          val.personaCui,
          val.vacunaId,
          val.fechaVacunacion
        );
        this.arr.push(vi);
        //llenar arreglo de vacunas y efectos
        this.buildEfectosVacunaMap(val.id);
      }
      this.personaService.getUnaPersona(cui).subscribe((res) => {});
    });
  }
  //mapa vacunas
  vacunasMap = new Map();
  //mapa de efectos de la vacuna
  efectosMap = new Map();
  //efactos de la vacuna cmap
  efectosVacunaMap = new Map<number, string[]>(); //key idVacunacion, valor del mapa un arreglo
  //funcion para construir mapa
  buildVacunaMap() {
    this.vacunaService.getVacunas().subscribe((res) => {
      this.vacunas = res;
      for (var val of this.vacunas.registros) {
        this.setVacunaMap(val);
      }
      this.getPersona(this.gcui);
    });
  }
  //funcion para construir mapa
  buildEfectosMap() {
    this.efectosService.getEfectos().subscribe((res2) => {
      this.efectos = res2;
      for (var val2 of this.efectos.registros) {
        this.setEfectosMap(val2);
      }
      this.buildVacunaMap();
    });
  }
  //funcion para construir mapa
  buildEfectosVacunaMap(id: number) {
    this.efectosVacunaService.getUnEfectoVacuna(id).subscribe((res3) => {
      this.efectos = res3;
      let arrEfectoVacuna = new Array();
      for (var val3 of this.efectos.registros) {
        if (val3.efectosId == 0) {
          arrEfectoVacuna.push('Otro: ' + val3.otro);
        } else {
          arrEfectoVacuna.push(this.efectosMap.get(val3.efectosId));
        }
      }
      console.log('lid ' + id + ' --> ' + arrEfectoVacuna);
      this.setEfectosVacunacionMap(id, arrEfectoVacuna);
    });
  }
  setVacunaMap(val: any) {
    this.vacunasMap.set(val.id, val.nombreVacuna);
  }
  setEfectosMap(val2: any) {
    this.efectosMap.set(val2.id, val2.efecto);
  }
  setEfectosVacunacionMap(id: number, val3: string[]) {
    this.efectosVacunaMap.set(id, val3);
  }
  onSubmit(cui: number) {
    this.router.navigate(['/registroEfecto/' + cui]);
  }

 async onDelete(id: number) {
    await this.delete1(id).then(res => {
      this.delete2(id);
    })
    
    //await this.redirect(id);
  }

  async delete1(id: number) {
    await this.efectosVacunaService.deleteEfectosVacuna(id) 
    await console.log('delete1');
  }
  async delete2(id: number) {
    await this.vacunacionesService.deleteVacunacion(id)
    await console.log('delete2');
  }
  async redirect(cui: number) {
    //await this.getAllOptions(questionID);
    await console.log('redirect');
  }
}
