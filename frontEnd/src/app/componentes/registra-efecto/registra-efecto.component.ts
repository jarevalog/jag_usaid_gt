import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PersonaService } from 'src/app/servicios/persona.service';
import { VacunaService } from 'src/app/servicios/vacuna.service';
import { EfectosService } from 'src/app/servicios/efectos.service';
import { genericOptsSelect } from 'src/app/model/gnericOptsSelect';
import { NgForm } from '@angular/forms';
import { VacunacionI } from 'src/app/model/vacunacion';
import { VacunacionesService } from 'src/app/servicios/vacunaciones.service';
import { EmbeddedTemplateAst } from '@angular/compiler';

declare function testDates(f1: string, f2: string): any;

@Component({
  selector: 'app-registra-efecto',
  templateUrl: './registra-efecto.component.html',
  styleUrls: ['./registra-efecto.component.css'],
})
export class RegistraEfectoComponent implements OnInit {
  //arrVacunaciones=new Array()
  vacunasOpt!: genericOptsSelect[];
  efectosOpt!: genericOptsSelect[];
  otro!: boolean;
  numDosis!: number;
  //selectedVacuna!:number

  constructor(
    private personaService: PersonaService,
    private activeRoute: ActivatedRoute,
    private vacunaService: VacunaService,
    private efectosService: EfectosService,
    private vacunacionesService: VacunacionesService,
    private router: Router
  ) {}
  gcui!: number;
  ngOnInit(): void {
    this.rEmbarazo = false;
    this.otro = false;
    const cui = this.activeRoute.snapshot.params.cui;
    this.gcui = cui;
    this.vacunasOpt = new Array();
    this.efectosOpt = new Array();
    console.log('onInit registroEfecto ' + cui);
    this.getPersona(cui);
    this.buildVacunaArr();
    this.buildEfectosArr();
  }
  persona: any; //objeto persona
  arrVacunaciones: any[] | undefined; //arreglo de vacunaciones
  getPersona(cui: number): void {
    this.personaService.getUnaPersona(cui).subscribe((res) => {
      this.persona = res;
      let auxArr: any[] = Array.of(this.persona.registro.jaVacunacions);
      this.arrVacunaciones = auxArr[0];
      this.numDosis = this.arrVacunaciones!.length;
    });
  }

  //funcion para construir mapa
  buildVacunaArr() {
    this.vacunaService.getVacunas().subscribe((res) => {
      let vacunas: any = res;
      for (var val of vacunas.registros) {
        this.vacunasOpt.push(
          new genericOptsSelect(val.id, val.nombreVacuna, '')
        );
      }
    });
  }
  postMap = new Map<string, any>();
  //funcion para construir mapa
  buildEfectosArr() {
    this.efectosService.getEfectos().subscribe((res2) => {
      let efectos: any = res2;
      for (var val2 of efectos.registros) {
        this.efectosOpt.push(
          new genericOptsSelect(val2.id, val2.efecto, 'chk' + val2.id)
        );
      }
    });
  }
  rEmbarazo!: boolean;
  arrCheks: any[] = new Array();
  async onSend(form: NgForm) {
    this.postMap = new Map<string, any>();
    this.arrCheks = new Array();

    //postMap.set('',
    if (this.rEmbarazo) this.postMap.set('embarazo', 1);
    else this.postMap.set('embarazo', 0);

    this.postMap.set('fechaParto', form.value.fechaParto);
    this.postMap.set('dosis', form.value.dosis);
    this.postMap.set('personaCui', this.gcui);
    this.postMap.set('vacunaId', parseInt(form.value.selectedVacuna));
    this.postMap.set('fechaVacunacion', form.value.fechaVacuna);

    let jsonObject: any = {};
    this.postMap.forEach((value, key) => {
      jsonObject[key] = value;
    });
    //guardar efectos vacunacion
    let map = new Map<string, string>();
    let vacunacionPost!: VacunacionI;
    for (var value in form.value) {
      map.set(value, form.value[value]);
    }

    map.forEach((value, key) => {
      if (key.startsWith('chk'))
        if (!(value.length == 0)) this.arrCheks.push(key.replace('chk', ''));
    });
    await this.validaciones(form.value.txtOtro);
    //guardar vacunacion
    if (this.messages.length <= 0) {
      this.vacunacionesService.postVacuna(
        jsonObject,
        this.arrCheks,
        this.otro,
        form.value.txtOtro
      );

      this.personaService.getUnaPersona(this.gcui).subscribe((res) => {
        this.router.navigate(['/resultado/' + this.gcui]);
        this.router.navigate(['/resultado/' + this.gcui]);
      });
    }
  }
  changeIsEmbarazo(isEmbarazo: any) {
    console.log('isEmbarazo>> ' + isEmbarazo.value);
    if (isEmbarazo.value == 'Si') {
      this.rEmbarazo = true;
    } else {
      this.rEmbarazo = false;
    }
  }
  regresar() {
    this.router.navigate(['/resultado/' + this.gcui]);
  }
  messages: string[] = new Array();
  validaciones(otroTxt: string) {
    this.messages = new Array();
    if (this.arrCheks.length <= 0) {
      this.messages.push(
        'Debe de seleccionar al menos un efecto secundario padecido por la vacunacion'
      );
    }
    console.log('validacion otro ' + this.otro + ' - ' + otroTxt.length);
    if (this.otro) {
      if (otroTxt.length == undefined) {
        this.messages.push(
          'Slecciono la casilla otro efectos secundario, favor de especificar'
        );
      }
    }

    var val = this.postMap.get('dosis');
    if (val.length <= 0) {
      this.messages.push('Debe seleccionar la dosis de la vacuna');
    } else {
      console.log('dosis ' + val);
      for (var val2 of this.arrVacunaciones!) {
        if (val == val2.dosis) {
          this.messages.push('Dosis ' + val2.dosis + ' ya fue registrada');
        }
      }
    }
    val = this.postMap.get('vacunaId');
    console.log(' vacuna ' + val);
    if (isNaN(val)) {
      this.messages.push('Debe seleccionar la vacuna ');
    }
    val = this.postMap.get('fechaVacunacion');
    console.log('fecha vacuna ' + val.length);
    if (val.length <= 0) {
      this.messages.push('La fecha vacuna es obligatoria ');
    }
    val = this.postMap.get('embarazo');
    console.log('embarazo ' + val);
    if (val == 1) {
      var val2 = this.postMap.get('fechaParto');
      if (val2.length <= 0) {
        this.messages.push(
          'Debe especificar la fecha aproximada en la que va a dar la luz '
        );
      } else {
        //los meses
        let date = new Date();
        let day = date.getDate();
        let month = date.getMonth() + 1;
        let year = date.getFullYear();
        let f1 = year + '/' + month + '/' + day;
        console.log('la fecha de ahora ' + f1);
        
        val2=val2.replace('-','/')
        val2=val2.replace('-','/')
        console.log('la fecha ' + val2);
        let meses: number = testDates(f1, val2);
        console.log('meses para el parto'+meses)
        if(meses>9){
          this.messages.push(
            'La fecha de parto sobre pasa los 9 meses probables en los que puede nacer el bebe '
          );
        }
      }
    }
  }
}
