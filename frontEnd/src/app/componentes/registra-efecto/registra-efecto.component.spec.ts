import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistraEfectoComponent } from './registra-efecto.component';

describe('RegistraEfectoComponent', () => {
  let component: RegistraEfectoComponent;
  let fixture: ComponentFixture<RegistraEfectoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistraEfectoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistraEfectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
