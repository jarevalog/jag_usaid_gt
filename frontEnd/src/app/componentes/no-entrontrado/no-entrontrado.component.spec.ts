import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoEntrontradoComponent } from './no-entrontrado.component';

describe('NoEntrontradoComponent', () => {
  let component: NoEntrontradoComponent;
  let fixture: ComponentFixture<NoEntrontradoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoEntrontradoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoEntrontradoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
