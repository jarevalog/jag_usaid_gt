-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema registrovacunacion
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema registrovacunacion
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `registrovacunacion` DEFAULT CHARACTER SET utf8 ;
USE `registrovacunacion` ;

-- -----------------------------------------------------
-- Table `registrovacunacion`.`ja_departamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `registrovacunacion`.`ja_departamento` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `departamento` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 23
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `registrovacunacion`.`ja_efectos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `registrovacunacion`.`ja_efectos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `efecto` VARCHAR(255) NOT NULL DEFAULT 'posible efecto',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 16
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `registrovacunacion`.`ja_vacuna`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `registrovacunacion`.`ja_vacuna` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre_vacuna` VARCHAR(255) NOT NULL,
  `procedencia` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `registrovacunacion`.`ja_municipio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `registrovacunacion`.`ja_municipio` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `municipio` VARCHAR(255) NOT NULL,
  `departamento_id` INT NOT NULL,
  PRIMARY KEY (`id`, `departamento_id`),
  INDEX `fk_municipio_departamento_idx` (`departamento_id` ASC) VISIBLE,
  CONSTRAINT `FK_ba0d33693bcbb02c920a14fe2d5`
    FOREIGN KEY (`departamento_id`)
    REFERENCES `registrovacunacion`.`ja_departamento` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 339
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `registrovacunacion`.`ja_persona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `registrovacunacion`.`ja_persona` (
  `cui` INT NOT NULL,
  `nombres` VARCHAR(255) NOT NULL,
  `apellidos` VARCHAR(255) NULL DEFAULT NULL,
  `fecha_nacimiento` DATETIME NULL DEFAULT NULL,
  `telefono` VARCHAR(45) NULL DEFAULT NULL,
  `municipio_id` INT NOT NULL,
  `sexo` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`cui`),
  INDEX `fk_persona_municipio1_idx` (`municipio_id` ASC) VISIBLE,
  CONSTRAINT `FK_72ce3a07c6e33e3d03df3deee65`
    FOREIGN KEY (`municipio_id`)
    REFERENCES `registrovacunacion`.`ja_municipio` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `registrovacunacion`.`ja_vacunacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `registrovacunacion`.`ja_vacunacion` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `embarazo` INT NOT NULL,
  `fechaParto` DATETIME NULL DEFAULT NULL,
  `dosis` INT NULL DEFAULT NULL,
  `persona_cui` INT NOT NULL,
  `vacuna_id` INT NOT NULL,
  `fecha_vacunacion` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_vacunacion_ja_persona1_idx` (`persona_cui` ASC) VISIBLE,
  INDEX `fk_vacunacion_ja_vacuna1_idx` (`vacuna_id` ASC) VISIBLE,
  CONSTRAINT `FK_1c3de43a54372d16738566f69f2`
    FOREIGN KEY (`vacuna_id`)
    REFERENCES `registrovacunacion`.`ja_vacuna` (`id`),
  CONSTRAINT `FK_481bf528509c4c3dc43ab4ca28b`
    FOREIGN KEY (`persona_cui`)
    REFERENCES `registrovacunacion`.`ja_persona` (`cui`))
ENGINE = InnoDB
AUTO_INCREMENT = 53
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `registrovacunacion`.`ja_efectos_vacunacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `registrovacunacion`.`ja_efectos_vacunacion` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `efectos_id` INT NOT NULL,
  `vacunacion_id` INT NULL DEFAULT NULL,
  `otro` VARCHAR(500) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_efectos_vacunacion_efectos1_idx` (`efectos_id` ASC) VISIBLE,
  INDEX `fk_efectos_vacunacion_vacunacion1_idx` (`vacunacion_id` ASC) VISIBLE,
  CONSTRAINT `FK_2b0e93c0769559eaa0768d9f159`
    FOREIGN KEY (`efectos_id`)
    REFERENCES `registrovacunacion`.`ja_efectos` (`id`),
  CONSTRAINT `FK_9bd0b519203dc378148d58a93c6`
    FOREIGN KEY (`vacunacion_id`)
    REFERENCES `registrovacunacion`.`ja_vacunacion` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 81
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `registrovacunacion`.`ja_usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `registrovacunacion`.`ja_usuario` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(16) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `registrovacunacion`.`tc_color`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `registrovacunacion`.`tc_color` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(255) NOT NULL,
  `fecha_creacion` DATETIME NOT NULL,
  `vigente` VARCHAR(255) NOT NULL,
  `fecha_inhabilitacion` DATETIME NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `registrovacunacion`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `registrovacunacion`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(255) NOT NULL,
  `lastname` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
